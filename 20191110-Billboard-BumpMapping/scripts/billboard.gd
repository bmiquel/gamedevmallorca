extends Sprite3D

class_name Billboard

export(NodePath) var target_camera

func _process(delta):
	self.set_rotation(get_node(target_camera).rotation)